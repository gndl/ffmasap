.PHONY: default build install uninstall test clean

default: build

dep:
	opam install -y --deps-only .

build:
	dune build src/ffmasap.exe

dbg:
	dune build src/ffmasap.bc
	cp ./_build/default/src/ffmasap.bc .

test:
	dune runtest -f

exec:
	dune exec src/ffmasap.exe

install:
	dune install

uninstall:
	dune uninstall

clean:
	dune clean
# Optionally, remove all files/folders ignored by git as defined
# in .gitignore (-X).
#git clean -dfXq
